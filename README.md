# Wardrobify

Team:

* Mateen Yeshova - Hats Microservice?
* Benjamin Stults - Shoes Microservice?

## Design

## Shoes microservice - Port 8080

Explain your models and integration with the wardrobe
microservice, here.

There are 2 models:
    Shoes - stores data to be displayed by the Shoes Microservice
    BinVO - model and foreign key of Shoes for polling data from the Wardrobe API to be used by the Shoes Microservice.

With the use of poller.py, the function get_bins() makes a request to the list of bins from Wardrobe API and updates or creates a BinVO model with "closet_name" and "import_href" properties, so that Shoes can be created and displayed by the Shoes Microservice.

There is also an interactive form to add new Shoe(s) which requires filling out the required fields, then hitting the Create button to submit it. Shoe(s) can also be deleted when looking at the Shoe List after verifying that you want to delete it.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
