import React, {useState, useEffect} from 'react'

function ShoesList(props) {

    const [shoes, setShoe] = useState([]);

    async function getShoes() {
        const response = await fetch("http://localhost:8080/api/shoes/");
        if (response.ok) {
            const {shoes} = await response.json();
            setShoe(shoes)
            } else {
                console.error('Error')
            }
        }

    useEffect(() => {
        getShoes();
    }, [])

    const onDelete=((id)=>{
        if (window.confirm("Do you want to remove?")) {
            fetch("http://localhost:8080/api/shoes/"+id,
            {method:"DELETE"}).then(()=>{

                window.location.reload();

            }).catch((err)=>{
                console.log(err.message)
            })
        }
    })


    if (shoes === undefined) {
        return null;
    }

    return (

        <table class="table table-dark table-striped-columns">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Manufacturer</th>
                    <th>Name</th>
                    <th>Color</th>
                    <th>Closet Name</th>
                    <th>Picture</th>
                    <th>Delete?</th>
                </tr>
            </thead>
            <tbody>
                {shoes.map(shoe => {
                    return (
                        <tr key={shoe.id}>
                            <td>{shoe.id}</td>
                            <td>{shoe.manufacturer}</td>
                            <td>{shoe.name}</td>
                            <td>{shoe.color}</td>
                            <td>{shoe.bin}</td>
                            <td>
                                <div class="text-center">
                                    <img src={shoe.picture_url} alt={shoe.name} className="img-thumbnail" style={{width: "75px", height: "75px"}} />
                                </div>
                            </td>
                            <td>
                                <button onClick={() => onDelete(shoe.id)}>Delete</button>
                            </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );

}

export default ShoesList
