import React, {useEffect,useState} from 'react'

function ShoeForm() {

    const [manufacturer, setManufacturer] = useState("")
    const [name, setName] = useState("")
    const [color, setColor] = useState("")
    const [pictureUrl, setPicture] = useState("")
    const [bin, setBin] = useState("")
    const [bins, setBins] = useState([])

    const manufacturerChange = (event) => {
        const value = event.target.value
        setManufacturer(value)
    }

    const nameChange = (event) => {
        const value = event.target.value
        setName(value)
    }

    const colorChange = (event) => {
        const value = event.target.value
        setColor(value)
    }

    const pictureUrlChange = (event) => {
        const value = event.target.value
        setPicture(value)
    }

    const binChange = (event) => {
        const value = event.target.value
        setBin(value)
    }

    const submitHandler = async (event) => {
        event.preventDefault()

        const newdata = {
          manufacturer,
          name,
          color,
          picture_url: pictureUrl,
          bin
        }

        const shoeUrl = 'http://localhost:8080/api/shoes/'
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(newdata),
            headers: {
                'content-type': 'application/json'
            }
        }

        const response = await fetch(shoeUrl, fetchConfig)
            if (response.ok) {
                setManufacturer('')
                setName('')
                setColor('')
                setPicture('')
                setBin('')
            }
        }

    const binData = async () => {
        const response = await fetch('http://localhost:8100/api/bins/')
        if (response.ok) {
            const data = await response.json()
            setBins(data.bins)
        }
    }

    useEffect (() => {
        binData()
    }, [])

        return (
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create new shoe(s)!</h1>
                <form onSubmit = {submitHandler} id="create-shoe-form">
                  <div className="form-floating mb-3">
                    <input onChange= {manufacturerChange} value= {manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                    <label htmlFor="manufacturer">Manufacturer</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange= {nameChange} value= {name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                    <label htmlFor="name">Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={colorChange} value= {color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                    <label htmlFor="color">Color</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={pictureUrlChange} value= {pictureUrl} placeholder="Picture" required type="text" name="picture_url" id="picture_url" className="form-control"/>
                    <label htmlFor="picture_url">Picture</label>
                  </div>
                  <div className="mb-3">
                    <select onChange={binChange} value= {bin} required name="bin" id="bin" className="form-select">
                    <option value="">Choose a bin</option>
                        {bins.map(bin => {
                          return (
                            <option key= {bin.closet_name} value={bin.href}>
                              {bin.closet_name}
                            </option>
                          );
                        })}
                    </select>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
            </div>
        )
}

export default ShoeForm
