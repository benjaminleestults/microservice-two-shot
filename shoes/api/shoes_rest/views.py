import json
# from django.shortcuts import render
from .models import Shoe, BinVO
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from django.http import JsonResponse


class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "import_href",
    ]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "name",
        "id",
        "color",
        "picture_url",
    ]

    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name}


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "name",
        "color",
        "picture_url",
        "bin",
    ]

    encoders = {
        "bin": BinVODetailEncoder(),
    }


# View for api/shoes/ for get=listshoes and post=createshoe
@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    # if GET - retrieve list of shoes
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    # else POST - create shoe
    else:
        content = json.loads(request.body)
        # try - try to create content
        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        # except - if no BinVO, raise error
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Bin"},
                status=400,
            )
    # create shoe content and display detail view for created shoe
    shoe = Shoe.objects.create(**content)
    return JsonResponse(
        shoe,
        encoder=ShoeDetailEncoder,
        safe=False,
    )


# View for api/shoes/<int:id>/ for get=detailshoe and delete=deleteshoe
@require_http_methods(["GET", "DELETE"])
def api_shoe_detail(request, id):
    # if GET then show detail
    if request.method == "GET":
        shoe = Shoe.objects.get(id=id)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    # elif DELETE then delete shoe at ID
    elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=id).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )
