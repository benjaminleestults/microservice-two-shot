from django.db import models


# BinVO class with properties as defined
class BinVO(models.Model):
    closet_name = models.CharField(
        max_length=100,
        null=True,
    )
    import_href = models.CharField(
        max_length=200,
        unique=True,
    )


# Shoe Class with properties as defined
# and __str__
class Shoe(models.Model):
    manufacturer = models.CharField(max_length=50)
    name = models.CharField(max_length=50)
    color = models.CharField(max_length=50)
    picture_url = models.URLField(null=True)

    bin = models.ForeignKey(
        BinVO,
        related_name="bins",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name
