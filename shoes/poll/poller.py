import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

# Import models from shoes_rest, here.
from shoes_rest.models import BinVO


def get_bins():
    # define URL for Bin Data
    binURL = "http://wardrobe-api:8000/api/bins/"
    # declare response
    response = requests.get(binURL)
    # json.load the response
    content = json.loads(response.content)
    # iterate through each bin and update/create data onto VO
    for bin in content["bins"]:
        BinVO.objects.update_or_create(
            import_href=bin["href"],
            defaults={
                "closet_name": bin["closet_name"],
            }
        )


def poll():
    while True:
        print('Shoes poller polling for data')
        try:
            get_bins()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
